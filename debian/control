Source: rust-hyper-rustls
Section: rust
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-cargo (>= 25),
 librust-futures-util-0.3-dev (>= 0.3.1) <!nocheck>,
 librust-http-0.2+default-dev <!nocheck>,
 librust-hyper-0.14+client-dev <!nocheck>,
 librust-hyper-0.14+default-dev <!nocheck>,
 librust-hyper-0.14+full-dev <!nocheck>,
 librust-hyper-0.14+http1-dev <!nocheck>,
 librust-hyper-0.14+runtime-dev <!nocheck>,
 librust-log-0.4+default-dev (>= 0.4.4) <!nocheck>,
 librust-rustls-0.20+logging-dev (>= 0.20.1) <!nocheck>,
 librust-rustls-0.20+tls12-dev (>= 0.20.1) <!nocheck>,
 librust-rustls-0.20-dev (>= 0.20.1) <!nocheck>,
 librust-rustls-native-certs-0.6+default-dev <!nocheck>,
 librust-rustls-pemfile-1+default-dev <!nocheck>,
 librust-tokio-1+default-dev <!nocheck>,
 librust-tokio-1+io-std-dev <!nocheck>,
 librust-tokio-1+macros-dev <!nocheck>,
 librust-tokio-1+net-dev <!nocheck>,
 librust-tokio-1+rt-multi-thread-dev <!nocheck>,
 librust-tokio-rustls-0.23+logging-dev <!nocheck>,
 librust-tokio-rustls-0.23+tls12-dev <!nocheck>,
 librust-tokio-rustls-0.23-dev <!nocheck>,
Maintainer: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/debian/rust-hyper-rustls.git
Vcs-Browser: https://salsa.debian.org/debian/rust-hyper-rustls
Homepage: https://github.com/ctz/hyper-rustls
Rules-Requires-Root: no

Package: librust-hyper-rustls-dev
Architecture: any
Multi-Arch: same
Depends:
 librust-http-0.2+default-dev,
 librust-hyper-0.14+client-dev,
 librust-rustls-0.20-dev (>= 0.20.1),
 librust-tokio-1+default-dev,
 librust-tokio-rustls-0.23-dev,
 ${misc:Depends},
Recommends:
 librust-hyper-rustls+default-dev (= ${binary:Version}),
Suggests:
 librust-hyper-rustls+http1-dev (= ${binary:Version}),
 librust-hyper-rustls+http2-dev (= ${binary:Version}),
 librust-hyper-rustls+logging-dev (= ${binary:Version}),
 librust-hyper-rustls+native-tokio-dev (= ${binary:Version}),
 librust-hyper-rustls+tls12-dev (= ${binary:Version}),
 librust-hyper-rustls+tokio-runtime-dev (= ${binary:Version}),
Provides:
 librust-hyper-rustls-0-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23.0-dev (= ${binary:Version}),
Description: rustls+hyper integration for pure rust HTTPS - Rust source code
 hyper-rustls is an integration
 between the rustls TLS stack and the hyper HTTP library.
 .
 This package contains the source
 for the Rust hyper-rustls crate,
 packaged for use with cargo and dh-cargo.

Package: librust-hyper-rustls+default-dev
Architecture: any
Multi-Arch: same
Depends:
 librust-hyper-rustls+http1-dev (= ${binary:Version}),
 librust-hyper-rustls+logging-dev (= ${binary:Version}),
 librust-hyper-rustls+native-tokio-dev (= ${binary:Version}),
 librust-hyper-rustls+tls12-dev (= ${binary:Version}),
 librust-hyper-rustls-dev (= ${binary:Version}),
 ${misc:Depends},
Provides:
 librust-hyper-rustls-0+default-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23+default-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23.0+default-dev (= ${binary:Version}),
Description: rustls+hyper integration for pure rust HTTPS - feature default
 hyper-rustls is an integration
 between the rustls TLS stack and the hyper HTTP library.
 .
 This metapackage enables feature "default"
 for the Rust hyper-rustls crate,
 by pulling in any additional dependencies needed by that feature.

Package: librust-hyper-rustls+http1-dev
Architecture: any
Multi-Arch: same
Depends:
 librust-hyper-0.14+http1-dev,
 librust-hyper-rustls-dev (= ${binary:Version}),
 ${misc:Depends},
Provides:
 librust-hyper-rustls-0+http1-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23+http1-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23.0+http1-dev (= ${binary:Version}),
Description: rustls+hyper integration for pure rust HTTPS - feature http1
 hyper-rustls is an integration
 between the rustls TLS stack and the hyper HTTP library.
 .
 This metapackage enables feature "http1"
 for the Rust hyper-rustls crate,
 by pulling in any additional dependencies needed by that feature.

Package: librust-hyper-rustls+http2-dev
Architecture: any
Multi-Arch: same
Depends:
 librust-hyper-0.14+http2-dev,
 librust-hyper-rustls-dev (= ${binary:Version}),
 ${misc:Depends},
Provides:
 librust-hyper-rustls-0+http2-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23+http2-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23.0+http2-dev (= ${binary:Version}),
Description: rustls+hyper integration for pure rust HTTPS - feature http2
 hyper-rustls is an integration
 between the rustls TLS stack and the hyper HTTP library.
 .
 This metapackage enables feature "http2"
 for the Rust hyper-rustls crate,
 by pulling in any additional dependencies needed by that feature.

Package: librust-hyper-rustls+logging-dev
Architecture: any
Multi-Arch: same
Depends:
 librust-hyper-rustls-dev (= ${binary:Version}),
 librust-log-0.4+default-dev (>= 0.4.4),
 librust-rustls-0.20+logging-dev (>= 0.20.1),
 librust-tokio-rustls-0.23+logging-dev,
 ${misc:Depends},
Provides:
 librust-hyper-rustls-0+logging-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23+logging-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23.0+logging-dev (= ${binary:Version}),
Description: rustls+hyper integration for pure rust HTTPS - feature logging
 hyper-rustls is an integration
 between the rustls TLS stack and the hyper HTTP library.
 .
 This metapackage enables feature "logging"
 for the Rust hyper-rustls crate,
 by pulling in any additional dependencies needed by that feature.

Package: librust-hyper-rustls+native-tokio-dev
Architecture: any
Multi-Arch: same
Depends:
 librust-hyper-rustls+tokio-runtime-dev (= ${binary:Version}),
 librust-hyper-rustls-dev (= ${binary:Version}),
 librust-rustls-native-certs-0.6+default-dev,
 ${misc:Depends},
Provides:
 librust-hyper-rustls-0+native-tokio-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23+native-tokio-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23.0+native-tokio-dev (= ${binary:Version}),
Description: rustls+hyper integration for pure rust HTTPS - feature native-tokio
 hyper-rustls is an integration
 between the rustls TLS stack and the hyper HTTP library.
 .
 This metapackage enables feature "native-tokio"
 for the Rust hyper-rustls crate,
 by pulling in any additional dependencies needed by that feature.

Package: librust-hyper-rustls+tls12-dev
Architecture: any
Multi-Arch: same
Depends:
 librust-hyper-rustls-dev (= ${binary:Version}),
 librust-rustls-0.20+tls12-dev (>= 0.20.1),
 librust-tokio-rustls-0.23+tls12-dev,
 ${misc:Depends},
Provides:
 librust-hyper-rustls-0+tls12-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23+tls12-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23.0+tls12-dev (= ${binary:Version}),
Description: rustls+hyper integration for pure rust HTTPS - feature tls12
 hyper-rustls is an integration
 between the rustls TLS stack and the hyper HTTP library.
 .
 This metapackage enables feature "tls12"
 for the Rust hyper-rustls crate,
 by pulling in any additional dependencies needed by that feature.

Package: librust-hyper-rustls+tokio-runtime-dev
Architecture: any
Multi-Arch: same
Depends:
 librust-hyper-0.14+runtime-dev,
 librust-hyper-rustls-dev (= ${binary:Version}),
 ${misc:Depends},
Provides:
 librust-hyper-rustls-0+tokio-runtime-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23+tokio-runtime-dev (= ${binary:Version}),
 librust-hyper-rustls-0.23.0+tokio-runtime-dev (= ${binary:Version}),
Description: rustls+hyper integration for pure rust HTTPS - feature tokio-runtime
 hyper-rustls is an integration
 between the rustls TLS stack and the hyper HTTP library.
 .
 This metapackage enables feature "tokio-runtime"
 for the Rust hyper-rustls crate,
 by pulling in any additional dependencies needed by that feature.
