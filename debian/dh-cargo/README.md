# dh-cargo fork

This is a fork of the debhelper script [dh-cargo],
based on git commit 5cc7f7b
(included with version 28 released 2021-11-07),
with these functional changes:

  * support debhelper option --sourcedirectory
  * fix use cargo_version (not Debian version) in crate path
  * generate cargo-checksum during install
  * stop blindly install all crate content:
    * omit any .git* files or directories
    * omit license files
    * omit debian/patches
    (see bug#880689)
  * use lockfile during build when available,
    or instead use debian/Cargo.lock when also available
  * use crates embedded below debian/vendorlibs when available
  * do build in target dh_auto_build
    (not in dh_auto_test)
  * check tests in target dh_auto_test

Also included is a slight fork of related cargo wrapper script,
based on git commit 7823074
(included with version 0.57.0-6 released 2022-04-10),
with these functional changes:

  * fix support relative path in CARGO_HOME, as documented
  * support DEB_BUILD_OPTIONS=terse
  * support DEB_BUILD_OPTIONS=noopt,
    consistently using release or bench profiles by default

[dh-cargo]: <https://salsa.debian.org/rust-team/dh-cargo/-/blob/master/cargo.pm>

[cargo]: <https://salsa.debian.org/rust-team/cargo/-/blob/debian/sid/debian/bin/cargo>


## Usage

In your source package,
copy directory `dh-cargo` to `debian/dh-cargo`
and edit `debian/rules` to something like this:

```
#!/usr/bin/make -f

# use local fork of dh-cargo and cargo wrapper
PATH := $(CURDIR)/debian/dh-cargo/bin:$(PATH)
PERL5LIB = $(CURDIR)/debian/dh-cargo/lib
export PATH PERL5LIB

%:
	dh $@ --buildsystem cargo --sourcedirectory=rustls
```


 -- Jonas Smedegaard <dr@jones.dk>  Mon, 27 Jun 2022 19:41:15 +0200
